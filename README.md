# lazy find
__Can't remember where is a file? Try the lf (lazy find) script for the easy way out.__

* Put the script in your $PATH 
* chmod +x

It's a simple frontend to the popular find or locate command, but with less typing and far less functionality.

For example, to find a file located in the current directory that contains the string foo in the file name:

```
$ lf foo
/home/klaatu/foo.txt
/home/klaatu/goodfood.list
/home/klaatu/tomfoolery.jpg
```

To find a file located in another directory with the string foo in the file name:

`$ lf --path ~/path/to/dir foo`

Original author: __Klaatu__

More info: https://opensource.com/article/20/2/find-file-script